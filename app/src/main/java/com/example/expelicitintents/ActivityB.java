package com.example.expelicitintents;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;


/**
 * Created by yodgorbek on 14.10.15.
 */
public class ActivityB extends ActionBarActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_b);

    }
}
